﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    class Block
    {
        public int x { get; set; }
        public int y { get; set; }      // position

        public Block()                  // constructor
        {
            x = 0;                      // starting position
            y = 0;
        }
    }
}

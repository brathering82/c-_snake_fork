﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    public partial class Form1 : Form
    {
        private List<Block>Snake = new List<Block>();       // create Snake character as list of blocks
        private Block food = new Block();                   // create a food object

        public Form1()
        {
            InitializeComponent();

            new Settings();                                 // load default settings

            TimerGame.Interval = 1000 / Settings.speed;     // Timer interval in ms - higher speed = shorter interval
            TimerGame.Tick += UpdateScreen;                 // set ticks of Timer
            TimerGame.Start();                              // start Timer

            StartGame();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void UpdateScreen(object sender, EventArgs e)
        {
            if (Settings.gameOver)
            {
                if (Input.KeyPressed(Keys.Enter))
                {
                    StartGame();
                }
                if (Input.KeyPressed(Keys.Escape))
                {
                    Application.Exit();
                }
            }
            else
            {                                                                                                                   // enable ArrowKeys and WASD controls
                if ((Input.KeyPressed(Keys.Right) || Input.KeyPressed(Keys.D)) && Settings.direction != Direction.left)         // if Right or D and not moving in opposite direction
                    Settings.direction = Direction.right;                                                                       // change direction
                else if ((Input.KeyPressed(Keys.Left) || Input.KeyPressed(Keys.A)) && Settings.direction != Direction.right)
                    Settings.direction = Direction.left;
                else if ((Input.KeyPressed(Keys.Up) || Input.KeyPressed(Keys.W)) && Settings.direction != Direction.down)
                    Settings.direction = Direction.up;
                else if ((Input.KeyPressed(Keys.Down) || Input.KeyPressed(Keys.S)) && Settings.direction != Direction.up)
                    Settings.direction = Direction.down;

                MoveCharacter();    // move snake
            }

            pbCanvas.Invalidate();  // delete everything on canvas for refresh -> calls pbCanvas_Paint
        }

        private void StartGame()
        {
            lblGameOver.Visible = false;                    // hide game over message

            new Settings();                                 // load default settings
            TimerGame.Interval = 1000 / Settings.speed;

            Snake.Clear();                                  // discard old parameters
            Block head = new Block();                       // create new Snake character as a single Block
            head.x = 10;                                    // place this Block
            head.y = 5;
            Snake.Add(head);                                // add this Block to game

            lblScore02.Text = Settings.score.ToString();    // update score

            PlaceFood();                                    // place a food item
        }

        private void PlaceFood()
        {
            int maxXpos = pbCanvas.Size.Width / Settings.width;     // determine maximum x position
            int maxYpos = pbCanvas.Size.Height / Settings.height;
            bool end = false;                                     // loop terminator

            do{
                Random rand = new Random();                     // create Random object
                food = new Block();                             // create food Block
                food.x = rand.Next(0, maxXpos);                 // set food to random position
                food.y = rand.Next(0, maxYpos);

                for (int i = 0; i < Snake.Count; i++)           // test for every block in snake character
                {
                    if (!((food.x == Snake[i].x) && (food.y == Snake[i].y)))    // if food position is NOT on a snake block
                        end = true;                                             // end loop
                }
            } while (!end);
        }

        private void pbCanvas_Paint(object sender, PaintEventArgs e)
        {
            Graphics canvas = e.Graphics;                   // tell program which graphics to use

            if (!Settings.gameOver)
            {
                Brush snakeColour;                          // for setting colour of character

                for(int i = 0; i < Snake.Count; i++)        // for every Block in character
                {
                    if (i == 0)
                        snakeColour = Brushes.Red;          // head
                    else
                        snakeColour = Brushes.DarkGreen;    // body

                    canvas.FillRectangle(snakeColour, new Rectangle(Snake[i].x * Settings.width, Snake[i].y * Settings.height, Settings.width, Settings.height)); // draw snake character
                    canvas.FillEllipse(Brushes.Black, new Rectangle(food.x * Settings.width, food.y * Settings.height, Settings.width, Settings.height));         // draw food item 
                }
            }
            else
            {
                string msgGameOver = "Game Over! \nFinal Score is " + Settings.score + "\nPress Enter for new game \nPress Esc to close application";    // set game over message
                lblGameOver.Text = msgGameOver;
                lblGameOver.Visible = true;
            }
        }

        private void MoveCharacter()
        {
            for (int i = Snake.Count - 1; i >= 0; i--)
            {
                if (i == 0)                                 // move head
                {
                    switch (Settings.direction)
                    {
                        case Direction.right:
                            Snake[i].x++; break;            // increment x position
                        case Direction.left:
                            Snake[i].x--; break;            // decrement x position
                        case Direction.up:
                            Snake[i].y--; break;
                        case Direction.down:
                            Snake[i].y++; break;
                    }

                    int maxXpos = pbCanvas.Size.Width / Settings.width;     // determine maximum x position
                    int maxYpos = pbCanvas.Size.Height / Settings.height;

                    if (Snake[i].x < 0 || Snake[i].y < 0 || Snake[i].x >= maxXpos || Snake[i].y >= maxYpos)     // if head is out of bounds
                    {
                        Die();
                    }

                    for (int j = 1; j < Snake.Count; j++)
                    {
                        if (Snake[i].x == Snake[j].x && Snake[i].y == Snake[j].y)   // if head bites any part of the body
                        {
                            Die();
                        }
                    }

                    if (Snake[0].x == food.x && Snake[0].y == food.y)  // if head bites food item
                    {
                        Eat();
                    }
                }
                else
                {
                    Snake[i].x = Snake[i - 1].x;            // move part of body to previous position of the previous part
                    Snake[i].y = Snake[i - 1].y;
                }
            }
        }

        private void Eat()
        {
            Block newBody = new Block();                    // generate a new body part
            newBody.x = Snake[Snake.Count - 1].x;           // set its position
            newBody.y = Snake[Snake.Count - 1].y;
            Snake.Add(newBody);                             // add body part to snake character

            Settings.score += Settings.points;              // refresh score
            lblScore02.Text = Settings.score.ToString();

            switch (Settings.score){                        // increase difficulty & rewards over time
                case 20:
                    Settings.speed = 20; Settings.points = 7; break;
                case 55:
                    Settings.speed = 25; Settings.points = 10; break;
                case 105:
                    Settings.speed = 30; Settings.points = 20; break;
                case 205:
                    Settings.speed = 50; Settings.points = 35; break;
            }

            TimerGame.Interval = 1000 / Settings.speed;     // set (new) speed

            PlaceFood();                                    // generate and place new food item
        }

        private void Die()
        {
            Settings.gameOver = true;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Input.ChangeState(e.KeyCode, true);             // set state of key to true on KeyDown
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            Input.ChangeState(e.KeyCode, false);            // reset state of key on KeyUp
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    public enum Direction { up, down, left, right};     // introduce directions

    class Settings
    {
        public static int width { get; set; }
        public static int height { get; set; }         // dimensions of blocks
        public static int speed { get; set; }
        public static int score { get; set; }
        public static int points { get; set; }         // worth of food item
        public static bool gameOver { get; set; }
        public static Direction direction { get; set; }

        public Settings()                               // constructor
        {
            width = 15;
            height = 15;
            speed = 15;
            score = 0;
            points = 5;
            gameOver = false;
            direction = Direction.right;
        }
    }
}

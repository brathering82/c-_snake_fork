﻿using System.Collections;
using System.Windows.Forms;

namespace Snake
{
    internal class Input
    {
        // load list of keyboard buttons
        private static Hashtable keyTable = new Hashtable();

        // check which button is pressed
        public static bool KeyPressed(Keys key)
        {
            if (keyTable[key] == null)              // return false if key is not in hashtable
            {
                return false;
            }
            return (bool) keyTable[key];            // return state of requested key
        }

        // set state of particular button
        public static void ChangeState(Keys key, bool state)
        {
            keyTable[key] = state;
        }
    }
}
